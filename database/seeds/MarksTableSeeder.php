<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class MarksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<=1500; $i++) {
            DB::table('marks')->insert([
                'longitude'=>(mt_rand(0, 360000000000000000)-180000000000000000)/1000000000000000,
                'latitude'=>(mt_rand(0, 360000000000000000)-180000000000000000)/1000000000000000,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ]);
        }
    }
}
