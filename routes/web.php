<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::post('/profile/{id}/update', 'ProfileController@update')->name('profile.update');

Route::prefix('/map')->middleware('auth')->group(function() {
    Route::get('/', 'HomeController@index');

    Route::resources(['users' => 'UserController', 'marks' => 'MarkController']);
    Route::get('/getmarks', 'MarkController@getmarks');
});

//chat
Route::post('messages', 'ChatController@getmessage');
Route::get('getuser', 'ChatController@getuser');
Route::get('getuser', 'ChatController@getuser');
