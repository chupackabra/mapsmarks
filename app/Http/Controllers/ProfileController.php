<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('profile', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|email',
            'password' =>'required'
            ]);
        User::where('id', $id)->update([
            'email' => $request->email,
            'password' => bcrypt($request->input('newpassword')),
            ]);
        return redirect()->back()->with('message', 'Вы успешно изменили свои данные!');
    }
}
