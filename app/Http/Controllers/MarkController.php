<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Mark;

class MarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marks = Mark::orderBy('id', 'asc')->paginate(100);
        return view('marks.index', compact('marks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marks.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['longitude' => 'required|max:20|min:5', 'latitude' => 'required|max:19|min:5']);
        $latitude = (float)$request->latitude;
        $longitude = (float)$request->longitude;

        if (($latitude <= 180 && $latitude >= -180 && $latitude != 0) && ($longitude <= 180 && $longitude >= -180 && $latitude != 0)) {
            $mark = new Mark();
            $mark->fill($request->all());
            $mark->save();
        } else {
            return redirect()->back()->with('mess', 'Не корректные форматы координат');
        }
        return redirect('map/marks')->with('mess', 'Новый геотег успешно создан!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mark = Mark::find($id);
        return view('marks.form', compact('mark'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(['longitude' => 'required|max:20|min:5', 'latitude' => 'required|max:19|min:5']);
        $latitude = (float)$request->latitude;
        $longitude = (float)$request->longitude;
        if (($latitude <= 180 && $latitude >= -180 && $latitude != 0) && ($longitude <= 180 && $longitude >= -180 && $latitude != 0)) {
            Mark::where('id', $id)->update(['longitude' => $longitude, 'latitude' => $latitude,]);
        }
        return redirect('map/marks')->with('mess', 'Геотег удачно изменен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Mark::destroy($id);
        return redirect('map/marks');
    }

    public function getmarks()
    {
        $marks = Mark::all();
        return new JsonResponse($marks);

    }
}
