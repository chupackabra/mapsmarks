<?php

namespace App\Http\Controllers;

use App\Events\Message;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function getmessage(Request $request)
    {
        $user = Auth::user()->name;
        event(new Message($request->body,  $user));

    }

    public function getuser()
    {
        $user = Auth::user()->name;
        return new JsonResponse($user);
    }
}
