<!doctype html>
<html >
<head>
    {{--<meta charset="utf-8">--}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>World Map @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=4044edd2-df7f-45d3-aa12-d4053764e392
" type="text/javascript"></script>



</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/map">На карту</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ">
            <li class="nav-item ">
                <a class="nav-link" href="{{route('profile')}}">Личный кабинет <span class="sr-only">(current)</span></a>
            </li>

            </li>
            @if(auth()->user()->id == 1)
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route ('users.index')}}">Пользователи<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route ('marks.index')}}">Метки <span class="sr-only">(current)</span></a>
                </li>
            @endif
        </ul>
    </div>
    <div class="navbar-nav justify-content-end">

        <form id="logout-form" class="" action="{{ route('logout') }}" method="POST" >
            @csrf
            <input type="submit" class="btn btn-secondary" value="Выйти">
        </form>
    </div>
</nav>
    <br>

    @yield('content')
    <br>


<script src="{{ asset('js/app.js')}}"></script>

</body>
</html>