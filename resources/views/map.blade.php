@extends('layouts.main')
@section('content')

<div id="map">
    <mapmark></mapmark>
</div>
<div id="chat">
    <chat-component></chat-component>
</div>

@endsection