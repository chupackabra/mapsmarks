@extends('layouts.main')
@section('content')
    <div class="container">
    <h1>Здравствуйте {{$user->name}}!</h1>

        <br>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('message'))
            <p class="text-success">
                {{session('message')}}
            </p>
        @endif
        <br>
        <p>Измените информацию о себе</p>
        <form action="{{route('profile.update', $user->id)}}" method="post">
            @csrf
            <div class="form-group mt-3">
                <label for="">Ваша почта:</label><br>
                <input class="form-control" name='email' type="text" value="{{$user->email }}" >
            </div>
            <div class="form-group mt-3">
                <label for="">Введите новый пароль:</label><br>
                <input class="form-control" name='newpassword' type="text" value="" >
            </div>
            <input type="submit" class="btn btn-info">
        </form>

    </div>
@endsection