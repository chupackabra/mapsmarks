@extends('layouts.main')
@section('content')
    <div class="container">
        <h1>Геотеги</h1>

        <a href="{{route('marks.create')}}" class="btn mt-3 mb-3 btn-success">Добавить геотег</a>

        <br>
        @if(session('mess'))
            <p class="text-success">
                {{session('mess')}}
            </p>
        @endif
        @if(session('error'))
            <p class="text-success">
                {{session('error')}}
            </p>
        @endif

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Id </th>
                <th>Широта</th>
                <th>Долгота</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($marks as $mark)
                <tr>
                    <td>{{$mark->id}}</td>

                    <td>{{$mark->latitude}} </td>
                    <td>{{$mark->longitude}} </td>

                    <td>
                        <a href="{{route('marks.edit', $mark->id)}}"  class="btn mb-2 btn-info">Изменить</a>
                        <form action="{{route('marks.destroy', $mark->id)}}" method="post">
                            @csrf
                            {{method_field('DELETE')}}
                            <input type="submit" class="btn btn-danger" value ='Удалить'/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>
        {{ $marks->links() }}
    </div>
@endsection