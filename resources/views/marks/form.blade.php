@extends('layouts.main')
@section('content')
    <div class="container">


        @if(isset($mark))
            <h1>Изменить геометку</h1>
        @else
            <h1>Добавить геометку</h1>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <br>
            <p class="text-info">Метки должны быть цифлом в формате "nn.nnnnnnnnnnnnnnn" 15 знаков после запятой без пробелов</p>
        <form action="{{isset($mark) ? route('marks.update', $mark->id): route('marks.store')}}" method="post" >
            @csrf
            @if(isset($mark))
                @method('PUT')
            @endif
            @if(session('mess'))
                <p class="text-danger">
                    {{session('mess')}}
                </p>
            @endif
            <div class="form-group mt-3">
                <label for="">Широта:</label><br>
                <p class="text-info">Диапазон чисел от -180.000000000000000 до 180.000000000000000</p>
                <input class="form-control" name='latitude' type="text" value="{{isset($mark) ? $mark->latitude : ''}}" >
            </div>


            <div class="form-group mt-3">
                <p class="text-info">Диапазон чисел от -180.000000000000000 до 180.000000000000000</p>
                <label for="">Долгота:</label><br>
                <input class="form-control" name='longitude'  type="text"  value ="{{isset($mark) ? $mark->longitude : ''}}">
            </div>

            @if(isset($mark))
                <input type="submit" class="mt-3 btn btn-success" value="Изменить">
            @else
                <input type="submit" class="mt-3 btn btn-success" value="Добавить ">
            @endif

        </form>

    </div>

@endsection