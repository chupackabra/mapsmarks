@extends('layouts.main')
@section('content')
    <div class="container">


            @if(isset($user))
                <h1>Изменить пользователя</h1>
            @else
                <h1>Добавить пользователя</h1>
            @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            <form action="{{isset($user) ? route('users.update', $user->id): route('users.store')}}" method="post" enctype = 'multipart/form-data' >
                @csrf
                @if(isset($user))
                    @method('PUT')
                @endif
                @if(session('mess'))
                    <p class="text-danger">
                        {{session('mess')}}
                    </p>
                @endif
                <div class="form-group mt-3">
                    <label for="">Имя:</label><br>
                    <input class="form-control" name='name' type="text" value="{{isset($user) ? $user->name : ''}}" >
                </div>


                <div class="form-group mt-3">
                    <label for=""> Почта:</label><br>
                    <input class="form-control" name='email'  type="text" value ="{{isset($user) ? $user->email : ''}}">
                </div>
                <div class="form-group mt-3">
                    <label for=""> Пароль:</label><br>
                    <input class="form-control" name='password'  type="text" value="">
                </div>

                @if(isset($user))
                    <input type="submit" class="mt-3 btn btn-success" value="Изменить">
                @else
                    <input type="submit" class="mt-3 btn btn-success" value="Добавить ">
                @endif

            </form>

    </div>

@endsection