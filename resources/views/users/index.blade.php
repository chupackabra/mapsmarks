@extends('layouts.main')
@section('content')
<div class="container">
    <h1>Пользователи</h1>

    <a href="{{route('users.create')}}" class="btn mt-3 mb-3 btn-success">Добавить пользователя</a>

    <br>
    @if(session('mess'))
        <p class="text-success">
            {{session('mess')}}
        </p>
    @endif
    @if(session('error'))
        <p class="text-success">
            {{session('error')}}
        </p>
    @endif

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Имя </th>
            <th>E-mail</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>

                <td>{{$user->email}} </td>

                <td>
                    <a href="{{route('users.edit', $user->id)}}"  class="btn mb-2 btn-info">Изменить</a>
                    <form action="{{route('users.destroy', $user->id)}}" method="post">
                        @csrf
                        {{method_field('DELETE')}}
                        <input type="submit" class="btn btn-danger" value ='Удалить'/>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>
</div>
    @endsection